
//creacion de modilo alumno
var Alumnos = (function () {
 var _alumnos = [];
 
 function _lista_de_alumnos() {
 return _alumnos;
 }
 
 function _agregar_nuevo_alumno(obj) {
 _alumnos.push(obj);
 }
 
 return {
 "lista_de_alumnos": _lista_de_alumnos,
 "agregar_nuevo_alumno": _agregar_nuevo_alumno
 };
 })();
 
 //modulo docente
 var Docente = (function () {
 var _docente = [];
 
 function _lista_de_docentes() {
 return _docente;
 }
 
 function _agregar(obj) {
 _docente.push(obj);
 }
 
 return {
 "lista_de_docentes": _lista_de_docentes,
 "agregar_docentes": _agregar_docentes
 };
 })();
  
   //modulo grupo
 var Grupo = (function () {
 var _grupo = [];
 
 function _lista_de_grupos() {
 return _grupo;
 }
 
 function _agregar_grupo(obj) {
 _grupo.push(obj);
 }
 
 return {
 "lista_de_grupos": _lista_de_grupos,
 "agregar_grupos": _agregar_grupos
 };
 })();
 
  //modulo Materias
 var Materias = (function () {
 var _materias = [];
 
 function _lista_de_materias() {
 return _materias;
 }
 
 function _agregar_materias(obj) {
 _materias.push(obj);
 }
 
 return {
 "lista_de_materias": _lista_de_materias,
 "agregar_materias": _agregar_materias
 };
 })();
 
